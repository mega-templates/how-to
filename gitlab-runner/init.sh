
GITLAB_TOKEN=${GITLAB_TOKEN}

while [ $# -gt 0 ]; do
    if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
    fi
  shift
done

if [ -z "$GITLAB_TOKEN" ]
then
      echo "GITLAB_TOKEN is empty"; exit 1
fi

curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"

dpkg -i gitlab-runner_amd64.deb

gitlab-runner uninstall

gitlab-runner install --working-directory /root --user root

service gitlab-runner restart

sudo gitlab-runner register -n --url https://gitlab.com/ --registration-token $GITLAB_TOKEN --executor shell --description "MainRunner" --locked="false"