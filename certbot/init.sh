#!/bin/bash

#Установка snap

sudo apt update
sudo apt install snapd
sudo snap install core
sudo snap refresh core

#Установка certbot
sudo apt-get remove certbot
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
