#!/bin/bash

#$1 - DOMAIN,DOMAIN2,DOMAIN3

if [ -z "$1" ]
then
      echo "DOMAIN_LIST is empty"; exit 1
fi

echo "Init dry-run certs with = $1"

sudo certbot certonly --standalone --dry-run -d $1