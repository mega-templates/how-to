
#Объявляем переменные - которые нам нужны
#Считываем параметры из команды запуска
REDIS_PASSWORD=${REDIS_PASSWORD}

while [ $# -gt 0 ]; do
    if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
    fi
  shift
done

if [ -z "$REDIS_PASSWORD" ]
then
      echo "REDIS_PASSWORD is empty"; exit 1
fi

#Создаем папку для редиски
mkdir -p ~/redis/data
mkdir -p ~/redis/conf
mkdir -p ~/redis/logs

#копируем docker-compose файл
cp docker-compose.yaml ~/redis/

#Копируем конфиг редиски
cp redis.conf ~/redis/conf/

#Создаем файл .env
echo "REDIS_PASSWORD=$REDIS_PASSWORD" >> ~/redis/.env

#Меняем права на папку РЕКУРСИВНО
chmod -R 777 ~/redis/

#Запускаем редиску
docker-compose -f ~/redis/docker-compose.yaml up -d 




