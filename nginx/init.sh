#!/bin/bash

# Два списка с DOMAIN=PORT
# ФОрмат списков: api.ekonvert.ru,dev.api.ekonvert.ru и ТД
FRONT=${FRONT}
BACK=${BACK}

while [ $# -gt 0 ]; do
    if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
    fi
  shift
done

echo -e "\nFront INCOMING: ${FRONT}"
echo -e "\nBack INCOMING: ${BACK}"

# Сплитуем массив по ,
IFS="," read -ra FRONT_ARRAY <<< $FRONT
IFS="," read -ra BACK_ARRAY <<< $BACK

echo -e "\nFRONT_ARRAY: ${FRONT_ARRAY}"

#Делаем пробежку по сплитованному массиву
for i in ${FRONT_ARRAY[*]}
do
  echo -e "\nFRONT_ARRAY element: ${i}"

  IFS="=" read -ra FRONT_SPLITTED_ARRAY <<< $i

  echo -e "\nFRONT_SPLITTED_ARRAY element: $FRONT_SPLITTED_ARRAY"

  echo "FRON DOMAIN =" ${FRONT_SPLITTED_ARRAY[0]}
  echo "FRON PORT =" ${FRONT_SPLITTED_ARRAY[1]}
done

# echo "Back array: ${BACK_ARRAY[@]}"

# for i in "${BACK_ARRAY[@]}"
# do
#    echo "$i"
# done

## declare an array variable
# declare -a arr=("element1" "element2" "element3")

# a_flag=''
# b_flag=''
# files=''
# verbose='false'

# print_usage() {
#   printf "Usage: ..."
# }

# while getopts 'abf:v' flag; do
#   case "${flag}" in
#     a) a_flag='true' ;;
#     b) b_flag='true' ;;
#     f) files="${OPTARG}" ;;
#     v) verbose='true' ;;
#     *) print_usage
#        exit 1 ;;
#   esac
# done