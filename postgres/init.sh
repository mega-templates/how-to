
#Объявляем переменные - которые нам нужны
#Считываем параметры из команды запуска
DB_USER=${DB_USER}
DB_PASSWORD=${DB_PASSWORD}

while [ $# -gt 0 ]; do
    if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
    fi
  shift
done

if [ -z "$DB_USER" ]
then
      echo "DB_USER is empty"; exit 1
fi

if [ -z "$DB_PASSWORD" ]
then
      echo "DB_PASSWORD is empty"; exit 1
fi

#Создаем папку для постгри
mkdir ~/postgres/

#копируем docker-compose файл
cp docker-compose.yaml ~/postgres/

#Создаем файл .env
echo "POSTGRES_PASSWORD=$DB_PASSWORD" >> ~/postgres/.env
echo "POSTGRES_USER=$DB_USER" >> ~/postgres/.env

#Меняем права на папку РЕКУРСИВНО
chmod -R 777 ~/postgres/

#Запускаем редиску
docker-compose -f ~/postgres/docker-compose.yaml up -d 




